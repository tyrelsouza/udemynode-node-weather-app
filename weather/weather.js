const request = require('request');

const API_KEY = "ae8dd4c9c3a690289fd7cfe125f50e84"

var getWeather = (latitude, longitude, callback) => {
  request({
    url: `https://api.darksky.net/forecast/${API_KEY}/${latitude},${longitude}`,
    json: true
  }, (error, response, body) => {
    if (!error && response.statusCode === 200){
      callback(undefined, {
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature
      });
    } else {
      callback("Unable to fetch weather");
    }
  });
}

module.exports = {
  getWeather
}
