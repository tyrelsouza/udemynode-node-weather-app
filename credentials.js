var GOOGLE_API_KEY = process.env.GOOGLE_API_KEY;
var DARKSKY_API_KEY = process.env.DARKSKY_API_KEY;

module.exports = {
  GOOGLE_API_KEY,
  DARKSKY_API_KEY
}
